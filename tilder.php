<?php

$kirby->set('template', 'tilder', __DIR__ . '/templates/tilder.php');
$kirby->set('blueprint', 'tilder', __DIR__ . '/blueprints/tilder.yml');

kirby()->hook(['panel.page.update'], function($page) {
  if ($page->template() == "tilder") {
    getPageExport($page);
  }
});

function getPageExport($page) {

  $apiURL = 'http://api.tildacdn.info/v1/getpageexport/?publickey=' . $page->public() . '&secretkey=' . $page->secret() . '&pageid=' . $page->pageid();

  $response =  json_decode(file_get_contents($apiURL));

  foreach ($response->result->images as $file) {
    f::write($page->root() . '/' . $file->to, file_get_contents("$file->from"), $append = false);
  }

  foreach ($response->result->css as $file) {
    f::write($page->root() . '/' . $file->to, file_get_contents("$file->from"), $append = false);
  }
  foreach ($response->result->js as $file) {
    f::write($page->root() . '/' . $file->to, file_get_contents("$file->from"), $append = false);
  }

  $content = $response->result->html;
  $tildaRoot = 'kirby_images_root';
  $contentWithPageRoot = str_replace($tildaRoot, $page->contentURL() . '/', $content);
  f::write($page->root() . '/' . 'content.html', $contentWithPageRoot, $append = false);

}

function addTilderPage($page) {

  foreach ($page->files() as $file) {
    if ($file->extension() == "js") {
      echo js($file->url());
    }
    if ($file->extension() == "css") {
      echo css($file->url());
    }
  }

  // Reseting Tilda's box-sizing
  // Local devserver version
  echo css('/site/plugins/tilder/assets/tilder.css');
  // Apache version
  echo css('/assets/plugins/tilder/tilder.css');

  if ($page->file('content.html')) {
    echo $page->file('content.html')->read();
  }

}
